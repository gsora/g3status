package beans

import (
	"fmt"
	"os/exec"
	"strings"
)

// CLIBean is a status bean that executes a command periodically.
type CLIBean struct {
	StatusBean

	command       cliCommand
	displayConfig *CLIBeanConfig
}

// CLIBeanConfig holds static session parameters for a new CLIBean
type CLIBeanConfig struct {
	DisplayConfig
	Command          string
	ShellInterpreter string
}

// NewCLIBean creates a new CLIBean with the config settings.
func NewCLIBean(config *CLIBeanConfig) *CLIBean {
	if config.Command == "" {
		config.Command = "echo \"you forgot to gimme something to do :(\""
	}

	c := CLIBean{
		command: cliCommand{
			Command:          config.Command,
			ShellInterpreter: config.ShellInterpreter,
		},
	}

	c.displayConfig = config

	return &c
}

// GetData executes the command assigned to a CLIBean
func (cl *CLIBean) GetData() {
	data, err := cl.command.execute()
	if err != nil {
		cl.SetError(err)
		return
	}

	cl.Color = cl.displayConfig.Color
	cl.FullText = fmt.Sprintf("%s%s", cl.displayConfig.Prefix, strings.TrimSpace(data))
}

// cliCommand holds informations about a single command (or shell expression/pipe) to be executed
// in a shell interpreter.
// ShellInterpreter can be set to whatever shell the user wants, either by providing its full path or
// by just naming it.
// The user selected shell must accept expressions via stdin.
type cliCommand struct {
	ShellInterpreter string
	Command          string
}

func (c cliCommand) execute() (string, error) {
	cmdrdr := strings.NewReader(c.Command)
	var command *exec.Cmd

	/* #nosec, this call is safe */
	if c.ShellInterpreter == "" {
		command = exec.Command("sh")
	} else {
		command = exec.Command(c.ShellInterpreter)
	}

	command.Stdin = cmdrdr
	output, err := command.CombinedOutput()
	if err != nil {
		return "", err

	}

	return string(output), nil
}
