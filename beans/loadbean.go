package beans

import (
	"fmt"
	"strconv"
	"unsafe"

	"golang.org/x/sys/unix"
)

// LoadBean is a status bean holding the current system load.
type LoadBean struct {
	StatusBean

	displayConfig *LoadBeanConfig
}

// LoadBeanConfig holds static session parameters for a LoadBean
type LoadBeanConfig struct {
	DisplayConfig
}

type loadavg struct {
	Ldavg  [3]uint32
	Fscale uint64
}

// NewLoadBean creates a new LoadBean.
func NewLoadBean(config *LoadBeanConfig) *LoadBean {
	lb := LoadBean{}
	lb.displayConfig = config
	return &lb
}

// GetData grabs the local system load.
func (l *LoadBean) GetData() {
	load, err := l.getLoad()
	if err != nil {
		l.SetError(fmt.Errorf("cannot fetch system load"))
		return
	}

	l.FullText = fmt.Sprintf("%s%s", l.displayConfig.Prefix, load)
	l.Color = l.displayConfig.Color
}

func (l LoadBean) getLoad() (string, error) {
	sc, err := unix.Sysctl("vm.loadavg")
	if err != nil {
		return "", err
	}
	byteRepr := []byte(sc)

	/*
		#nosec, this pointer conversion is safe: comes from error-checked
		unix.Sysctl.
	*/
	load := *(*loadavg)(unsafe.Pointer((&byteRepr[0])))

	floatLoad := float64(load.Ldavg[0]) / float64(load.Fscale)
	strLoad := strconv.FormatFloat(floatLoad, 'f', 2, 32)

	return strLoad, nil
}
