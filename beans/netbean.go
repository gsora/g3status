package beans

import (
	"fmt"
	"net"
)

// NetBean is a status bean holding network devices data.
type NetBean struct {
	StatusBean

	displayConfig *NetBeanConfig
}

// NetBeanConfig holds static session parameters for a NetBean
type NetBeanConfig struct {
	DisplayConfig
	Wifi         bool
	NetInterface string
	DownColor    string
}

// TODO: add different color for down and up interface status

// NewNetBean creates a new NetBean with config settings.
func NewNetBean(config *NetBeanConfig) *NetBean {
	nb := NetBean{}

	if config.NetInterface == "" {
		firstIface, err := getFirstInterface()
		if err != nil {
			nb.SetError(err)
			return nil
		}

		config.NetInterface = firstIface.Name
	}

	if config.DownColor == "" {
		config.DownColor = ErrorColor
	}

	nb.displayConfig = config

	return &nb
}

// GetData grabs all the needed data from a network device, following n.displayConfig directives.
func (n *NetBean) GetData() {
	var err error
	var text string

	iface, err := net.InterfaceByName(n.displayConfig.NetInterface)
	if err != nil {
		n.SetError(err)
		return
	}

	addresses, err := iface.Addrs()
	if err != nil {
		n.SetError(err)
		return
	}

	if n.displayConfig.Wifi {
		ssid, err := getSSIDFreeBSD(n.displayConfig.NetInterface)
		if err != nil {
			n.SetError(err)
			return
		}
		text = fmt.Sprintf("%s%s (%s): ", n.displayConfig.Prefix, n.displayConfig.NetInterface, ssid)
	} else {
		text = fmt.Sprintf("%s%s: ", n.displayConfig.Prefix, n.displayConfig.NetInterface)
	}
	if iface.Flags&net.FlagUp == 0 {
		n.FullText = fmt.Sprintf("%s%s: down", n.displayConfig.Prefix, n.displayConfig.NetInterface)
		n.Color = n.displayConfig.DownColor
		return
	}

	for index, ip := range addresses {
		text = text + ip.String()
		if !(index+1 == len(addresses)) {
			text = text + ", "
		}
	}

	n.FullText = text
	n.Color = n.displayConfig.Color
}

func getFirstInterface() (net.Interface, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return net.Interface{}, err
	}

	return interfaces[0], nil
}
