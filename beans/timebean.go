package beans

import (
	"fmt"
	"time"
)

// TimeBean is a status bean holding time.
type TimeBean struct {
	StatusBean

	displayConfig *TimeBeanConfig
}

// TimeBeanConfig holds static session parameters for a TimeBean
type TimeBeanConfig struct {
	DisplayConfig
	Format string
}

// NewTimeBean creates a new TimeBean.
func NewTimeBean(config *TimeBeanConfig) *TimeBean {
	t := TimeBean{}
	if config.Format == "" {
		config.Format = "January 2, 2006 - 3:04 PM"
	}

	t.displayConfig = config

	return &t
}

// GetData formats current time using the user provided time format string,
// as per https://golang.org/pkg/time/#Time.Format.
func (t *TimeBean) GetData() {
	t.FullText = fmt.Sprintf("%s%s", t.displayConfig.Prefix, t.getTime())
	t.Color = t.displayConfig.Color
}

func (t TimeBean) getTime() string {
	return time.Now().Format(t.displayConfig.Format)
}
