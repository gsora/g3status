package beans

// Keywords is a map containing all the configuration keywords for a single
// bean, identified by its name.
type Keywords map[string][]string

var (
	// BeansKeywords maps a bean's name to its configuration keywords.
	BeansKeywords = Keywords{
		"net_bean": []string{
			"interface",
			"wifi",
			"downcolor",
		},
		"cli_bean": []string{
			"command",
			"shell",
		},
		"time_bean": []string{
			"format",
		},
		"general": []string{
			"color",
			"prefix",
		},
	}
)
