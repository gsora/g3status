package beans

/*
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <net/if_media.h>
#include <net80211/ieee80211.h>
#include <net80211/ieee80211_ioctl.h>
#include <unistd.h>
#define IW_ESSID_MAX_SIZE IEEE80211_NWID_LEN

struct ssid_payload {
	char *buff;
	int buff_len;
};
typedef struct ssid_payload ssid_payload;

ssid_payload *get_SSID(const char *interface) {
	int s, inwid;
	struct ieee80211req na;
	size_t len;
	ssid_payload *sp;

	sp = malloc(sizeof(ssid_payload));
	sp->buff = malloc(1024*sizeof(char));

	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
		return NULL;

	memset(&na, 0, sizeof(na));

	strlcpy(na.i_name, interface, sizeof(na.i_name));
	na.i_type = IEEE80211_IOC_SSID;
	na.i_data = sp->buff;
	na.i_len = IEEE80211_NWID_LEN + 1;
	if ((inwid = ioctl(s, SIOCG80211, (caddr_t)&na)) == -1) {
		close(s);
		return NULL;
	}
	if (inwid == 0) {
		if (na.i_len <= IEEE80211_NWID_LEN)
		len = na.i_len + 1;
		else
		len = IEEE80211_NWID_LEN + 1;
	} else {
		close(s);
	}

	sp->buff_len = strlen(sp->buff);
	return sp;
}

*/
import "C"

import (
	"fmt"
	"unsafe"
)

func getSSIDFreeBSD(wifiIface string) (string, error) {
	ifCString := C.CString(wifiIface)
	sp := C.get_SSID(ifCString)
	if sp == nil {
		return "", fmt.Errorf("%s is not a wifi capable network interface", wifiIface)
	}
	defer C.free(unsafe.Pointer(ifCString))
	defer C.free(unsafe.Pointer(sp))
	ssid := C.GoStringN(sp.buff, sp.buff_len)

	return ssid, nil
}
