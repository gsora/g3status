package beans

import (
	"encoding/json"
	"fmt"
)

// StatusBean is the base bean, every other bean will be implemented by extending
// it.
type StatusBean struct {
	Name     string `json:"name"`
	Instance string `json:"instance,omitempty"`
	Color    string `json:"color,omitempty"`
	Markup   string `json:"markup"`
	FullText string `json:"full_text"`
	Align    string `json:"align"`
}

// GetData on a StatusBean will return error, we're not supposed to use that.
func (s *StatusBean) GetData() {
	s.FullText = "error: you're not supposed to use a StatusBean directly"
	s.Color = "#FF0000"
}

// SetError sets the color and text to error values.
func (s *StatusBean) SetError(err error) {
	s.FullText = fmt.Sprintf("error: %s", err.Error())
	s.Color = ErrorColor
}

// Status is a set of StatusBeans.
type Status []BeanBehavior

func (s Status) String() (string, error) {
	for _, bean := range s {
		bean.GetData()
	}

	data, err := json.Marshal(s)
	if err != nil {
		return "", err
	}

	return string(data) + ",", nil
}

// BeanBehavior is the interface that tells what a bean will do.
type BeanBehavior interface {
	GetData()
}

// DisplayConfig holds informations about specific general settings
// common to all beans, like color or the text prefix.
type DisplayConfig struct {
	Color  string
	Prefix string
}
