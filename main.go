package main

import (
	"flag"
	"fmt"
	"log"
	"os/user"
	"path"
	"time"

	"gitlab.com/gsora/g3status/config"
)

const (
	i3barJSONHeader string = "{\"version\":1}\n["
)

var (
	cfg            *config.Config
	configFilePath string
	err            error
)

func main() {
	setupCLIParams()
	cfg, err = config.LoadConfig(configFilePath)
	if err != nil {
		log.Fatal(err)
	}

	// first things first, send the JSON header
	fmt.Println(i3barJSONHeader)

	ticker := time.NewTicker(cfg.UpdateInterval)
	tickPrinting(ticker)
}

func tickPrinting(ticker *time.Ticker) {
	// print directly the first time we run
	printJSON()

	// then print each tick
	for range ticker.C {
		printJSON()
	}
}

func printJSON() {
	data, err := cfg.Beans.String()
	if err != nil {
		// if we have an error, stall and exit
		log.Fatal(err)
	}

	fmt.Println(data)
}

func setupCLIParams() {
	user, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	home := user.HomeDir
	cfgDefaultPath := path.Join(home, ".config/g3status.toml")
	flag.StringVar(&configFilePath, "config", cfgDefaultPath, "TOML configuration file path")
	flag.Parse()
}
