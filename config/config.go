package config

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"

	"github.com/pelletier/go-toml"
	"gitlab.com/gsora/g3status/beans"
)

// Config holds g3status configuration instructions.
type Config struct {
	Beans          *beans.Status
	UpdateInterval time.Duration
}

// LoadConfig returns a *Config by loading a file from path.
func LoadConfig(path string) (*Config, error) {
	defer handlePanic()

	c, err := toml.LoadFile(path)
	if err != nil {
		return nil, err
	}

	var cfg Config
	var finalBeans beans.Status

	// get all the beans
	beansGroup := c.Get("beans").([]*toml.Tree)

	for index, bean := range beansGroup {
		settings := bean.ToMap()
		typ, found := settings["type"]
		if !found {
			return nil, fmt.Errorf("bean #%d does not have a type", index)
		}

		var b beans.BeanBehavior
		detectTypeAndInit(&b, typ.(string), settings)

		finalBeans = append(finalBeans, b)
	}

	ui := int64(1)
	if uiIface := c.Get("general.update_interval"); uiIface != nil {
		ui = uiIface.(int64)
	}

	cfg.Beans = &finalBeans
	cfg.UpdateInterval = time.Duration(ui) * time.Second
	return &cfg, nil
}

// detectTypeAndInit detects a bean's type and proceeds to create the correct one
func detectTypeAndInit(b *beans.BeanBehavior, typ string, settings map[string]interface{}) {
	switch typ {
	case "time":
		*b = createTimeBean(settings)
	case "load":
		*b = createLoadBean(settings)
	case "net":
		*b = createNetBean(settings)
	case "cli":
		*b = createCLIBean(settings)
	}
}

func createCLIBean(conf map[string]interface{}) (cb *beans.CLIBean) {
	var command string
	var shell string

	if cdata, ok := conf["command"]; ok {
		command = cdata.(string)
	}

	if cdata, ok := conf["shell"]; ok {
		shell = cdata.(string)
	}

	cbc := beans.CLIBeanConfig{
		Command:          command,
		ShellInterpreter: shell,
	}
	cb = beans.NewCLIBean(&cbc)
	setGeneralTags(&(cbc.DisplayConfig), conf)
	return

}

func createTimeBean(conf map[string]interface{}) (tb *beans.TimeBean) {
	tbc := beans.TimeBeanConfig{}

	if format, ok := (conf["format"]); ok {
		tbc.Format = format.(string)
	}

	tb = beans.NewTimeBean(&tbc)
	setGeneralTags(&(tbc.DisplayConfig), conf)
	return
}

func createLoadBean(conf map[string]interface{}) (lb *beans.LoadBean) {
	lbc := beans.LoadBeanConfig{}
	lb = beans.NewLoadBean(&lbc)

	if len(conf) < 1 {
		return
	}

	setGeneralTags(&(lbc.DisplayConfig), conf)
	return
}

func createNetBean(conf map[string]interface{}) (nb *beans.NetBean) {
	var iface string
	var wifi bool
	var downColor string

	if cdata, ok := conf["interface"]; ok {
		iface = cdata.(string)
	}

	if cdata, ok := conf["wifi"]; ok {
		wifi = cdata.(bool)
	}

	if cdata, ok := conf["downcolor"]; ok {
		downColor = cdata.(string)
	}

	nbc := beans.NetBeanConfig{
		NetInterface: iface,
		Wifi:         wifi,
		DownColor:    downColor,
	}

	nb = beans.NewNetBean(&nbc)

	setGeneralTags(&(nbc.DisplayConfig), conf)
	return
}

func setGeneralTags(i interface{}, conf map[string]interface{}) {
	// Shame.
	v := reflect.ValueOf(i).Elem()

	// set general tags
	for _, gt := range beans.BeansKeywords["general"] {
		f := v.FieldByName(strings.Title(gt))
		if f != (reflect.Value{}) {
			if data, ok := conf[gt]; ok {
				f.SetString(data.(string))
			}
		}
	}
}

// There could be panics due to the (rather unclean) way we
// handle configuration files: at least print them/
func handlePanic() {
	if r := recover(); r != nil {
		var err error
		switch x := r.(type) {
		case string:
			err = errors.New(x)
		case error:
			err = x
		default:
			err = errors.New("unknown panic")
		}
		log.Println("malformed configuration file, please check the example provided with g3status")
		log.Fatal("error message: ", err)
	}
}
