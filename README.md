# `g3status`, a rather incomplete replacement for `i3status` [![Go Report Card](https://goreportcard.com/badge/gitlab.com/gsora/g3status)](https://goreportcard.com/report/gitlab.com/gsora/g3status)

![](screen.png)

`g3status` aims to replace - at least partially - `i3status` in its functionality.

This means that `g3status` is not a complete bar replacement like [`polybar`](https://github.com/jaagr/polybar), it's just another data source for [`i3bar`](https://i3wm.org/i3bar/)[^1]

`g3status` is written in Go.

It has been tested on FreeBSD only, it will most probably not even build on Linux because it's using some FreeBSD-specific `ioctl`'s to grab Wi-Fi informations and system load - this is an issue I'm actively working on, patches accepted!

## Features

`g3status` has been written with simplicity (and angery torwards `i3status`) in mind.

It only provides four basic **beans**[^2] (blocks):



 - `time`: displays time
 - `load`: fetches and displays system load
 - `net`: displays network interfaces informations
 - `cli`: executes shell commands

The simplicity of `g3status` resides in the fact that via a handful of shell oneliners, the user will be able to customize the content of its bar - no need to wrap `i3status` in some shady/dirty shell script, or to overengineer the data provider itself with features that most probably will never even be used.

## Configuration

`g3status` reads a [`toml`](https://github.com/toml-lang/toml)-compliant configuration file, with a well-defined structure.

You can let it load a configuration file from an arbitrary path via the `-config` parameter, or let it load from the default configuration path, which is `~/.config/g3status.toml`.

Bean order matters: their declarations will be evaluated in the order you list them, and added to the bar from **left to right**.

This means that the first bean declaration is the leftmost bean rendered on the bar.

You can find a configuration file example in the `config_example.toml` file.

## Installation

`g3status` is `go get`-able - the `master` branch of this repository will always point to the latest stable release.

Assuming you have correctly configured a Go environment:

```
$ go get gitlab.com/gsora/g3status
```

`g3status` will then be installed in `$GOPATH/bin`.

## Running `g3status` in `i3bar`

To run `g3status` in an `i3bar` instance, you must edit your `i3` configuration file, and specify the `status_command` parameter:

```
bar {
	# your bar configuration goes here
	status_command g3status # an absolute path is okay too
}
```


[^1]: it in fact speaks the `i3bar` JSON protocol syntax
[^2]: yes, it's a rather bad name.
